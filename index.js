console.log("Hello world");




//---------------------------------------------




// EXPONENT OPERATOR


console.log("---------------- EXPONENT OPERATOR")


const firstNum = 8 ** 2;
console.log(firstNum);





// TO THE POWER OPERATOR (same result with the Exponent Operator)


console.log("---------------- TO THE POWER OPERATOR")



const secondNum = Math.pow(8, 2);
console.log(secondNum);



//---------------------------------------------



// TEMPLATE LITERALS




/*

- Allows to write strings without using the concatenation operator (+)

- Greatly helps with code readability

*/



// PRE-TEMPLATE LITERAL - STRING


// Uses single qoute ('') or double qoute ("")



console.log("---------------- PRE-TEMPLATE LITERAL - STRING")


let name = "John"; 

// concatenation

let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: \n" + message);



// \n - is used to bring the next statement down





//---------------------------------------------


// TEMPLATE LITERAL - STRING


// Uses backticks (``)


/*

- Template literals allows us to write strings with embedded JavaScript expressions

- expressions are any valid unit of code that resolves to a value

- "${}" are used to include JavaScript expressions in strings using template literals

*/



console.log("---------------- TEMPLATE LITERAL - STRING")


// let name = "John"; 


message = `Hello ${name}! Welcome to Programming!`;
console.log(`Message with template literals: \n${message}`);




// ANOTHER EXAMPLE


const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`
console.log(anotherMessage);





// ANOTHER EXAMPLE - INTEREST RATE


console.log("---------------- TEMPLATE LITERAL - STRING interest rate")



const interestRate = .1;
const principal = 100;

console.log(`The interest of your savings account is ${principal*interestRate}`);







//---------------------------------------------


// ARRAY DE-STRUCTURING


/*

- Allows us to unpack elements in an array into distinct variables	

- Allows us to name array elements with variables, instead of using index numbers

- Helps with code readability

Syntax
        let/const [variableName, variableName, variableName] = array;

*/




// PRE-ARRAY DE-STRUCTURING



console.log("---------------- PRE-ARRAY DE-STRUCTURING")


const fullName = ["Juan", "Dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you`);





//---------------------------------------------


// ARRAY DE-STRUCTURING



console.log("---------------- ARRAY DE-STRUCTURING")


// const fullName = ["Juan", "Dela", "Cruz"];


const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`);






//---------------------------------------------


// OBJECT DE-STRUCTURING



/*

- Allows us to unpack properties/keys of objects into distinct variables

- Shortens the syntaxa for accessing preperties from objects

- Syntax
    let/const {propertyVariableName, propertyVariableName, propertyVariableName} = object;

- Note - Rule:
    the variableName to be assigned in destructuring should be the same as the propertyName

*/ 




// PRE-OBJECT DE-STRUCTURING (using dot notation)


console.log("---------------- PRE-OBJECT DE-STRUCTURING")


const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again`);






//---------------------------------------------


// OBJECT DE-STRUCTURING



console.log("---------------- OBJECT DE-STRUCTURING")


const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`);







//---------------------------------------------


// ARROW FUNCTIONS


/*


- Compact alternative syntax to traditional functions

- Useful for code snippets where creating function will not be reused in any other portion of the code

- Adheres to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name of function that will only be used in certain code snippets

Syntax
    const variableName = () => {
        console.log("");
    }

*/



console.log("---------------- ARROW FUNCTIONS")



const hello = () => {
    console.log("Hello world");
}

hello();




// ANOTHER EXAMPLE



const printFullName = (firstName, middleInitial, lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`);

}

printFullName("John", "D", "Smith");







//---------------------------------------------


// IMPLICIT RETURN STATEMENT


/*

- There are instances when you can omit the 'return' statement

- This works because without the 'return' statement JavaScript implicitly adds it for the result of the function

*/




// PRE-ARROW FUNCTION (implicit return statement)


/*

EXAMPLE WITH RETURN STATEMENT

const add = (numA, numB) => {
        return numA + numB;
    }

let total = add(1,2);
    console.log(total);
*/



console.log("---------------- PRE-ARROW FUNCTION (implicit return statement)")




const add = (numA, numB) => numA + numB;

let total = add(1,2);
console.log(total);






//---------------------------------------------


console.log("---------------- PRE-ARROW FUNCTION (with loops)")



const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
    console.log(`${student} is a student.`);
})







//---------------------------------------------


console.log("---------------- PRE-ARROW FUNCTION (forEach)")



students.forEach((student) => {
    console.log(`${student} is a student.`);
})







//---------------------------------------------


// DEFAULT FUNCTION ARGUMENT VALUE


// Provides a default argument value if none is provided when the function is invoked




console.log("---------------- DEFAULT FUNCTION ARGUMENT VALUE")



const greet = (name = 'User') => {
    return `Good morning, ${name}`
}

console.log(greet("John"));
console.log(greet());






//---------------------------------------------


// CLASS-BASED OBJECT BLUEPRINTS



/*

- Allows creation/instantiation of objects using classes as blueprint

Syntax:
    class className{
        constructor(objectPropertA, objectPropertyB){
            this.objectPropertyA = objectPropertyB
            this.objectPropertyB = objectPropertyB 
        }
    }

*/



console.log("---------------- CLASS-BASED OBJECT BLUEPRINTS")
console.log("---------------- Instantiating an object - no valueName")


class Car {
    constructor(brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}




console.log("---------------- Instantiating an object using dot notation")



const myCar = new Car();
console.log(myCar);


myCar.brand = "Ford";
myCar.name = "Range Raptor"
myCar.year = 2021;
console.log(myCar);





//---------------------------------------------




console.log("---------------- Instantiating an object with valueName")


const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);




